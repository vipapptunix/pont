import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  baseUrl: string = "http://appgrowthcompany.com:3125/";

  constructor(private http:HttpClient) { }



  Login(body)
  {
    return this.http.post<any>(`${this.baseUrl}admin/login`,body)
  }
 
  Allusers(body)
  {
    return this.http.post<any>(`${this.baseUrl}admin/getusers`,body)
  }

  deleteUser(data)
  {
    return this.http.post<any>(`${this.baseUrl}admin/deleteUser`,data)
  }

  disabledUsers(data)
  {
    return this.http.post(`${this.baseUrl}admin/disableUser`,data)
  }

  getAllcsvUSer()
  {
    return this.http.get<any>(`${this.baseUrl}admin/getUsersCSV`)
  }

  submitEdituser(data)
  {
    return this.http.post<any>(`${this.baseUrl}admin/editUser`,data)
  }

  viewUserdata(data)
  {
    return this.http.get<any>(`${this.baseUrl}admin/viewUser`,data)
  }
  forgotEmail(data)
{
  return this.http.post<any>(`${this.baseUrl}user/forgotpassword`,data)
}
}
