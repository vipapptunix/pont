import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { saveAs } from 'file-saver';
import { FormGroup, FormControl } from '@angular/forms';
import { PageEvent } from '@angular/material/paginator';
import { ServiceService } from 'src/service.service';
import * as moment from 'moment';

@Component({
  selector: 'app-basic-table',
  templateUrl: './basic-table.component.html',
  styleUrls: ['./basic-table.component.scss']
})

export class BasicTableComponent implements OnInit {

  public userdata: any;
  userlength: any;
  userid: any;

  Editprofile = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    userName: new FormControl(''),
    email: new FormControl(''),
    phone: new FormControl(''),
    gender: new FormControl(''),
    dob: new FormControl(''),
    bio: new FormControl(' '),
    id: new FormControl('')
   
  });
  Edituserdata: any;

  constructor(private modalService: NgbModal, private SrvAuth: ServiceService) { }

  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [5, 10, 25, 100];

  // MatPaginator Output
  pageEvent: PageEvent;

  setPageSizeOptions(setPageSizeOptionsInput: string) {
    if (setPageSizeOptionsInput) {
      this.pageSizeOptions = setPageSizeOptionsInput.split(',').map(str => +str);
    }
  }

  ngOnInit() {
    this.getAllUsers('', '')
  }


  getAllUsers(page, size) {
    const data =
    {
      "past": 'total',
      "page": page ? page : '1',
      "size": size ? size : '10'
    }
    this.SrvAuth.Allusers(data).subscribe((res: any) => {
      this.userdata = res.data
      this.userlength = res.data.length;
      
    })
  }

  pageeventvalue(e) {

    this.getAllUsers(e.pageIndex, e.pageSize)
  }


  generateUsercsv() {
    // this.SrvAuth.getAllcsvUSer().subscribe((response: any) => {
    //   // if(!response.status) this.errorToast(response.message)
    //   console.log(response);
      saveAs(`http://appgrowthcompany.com:3125/admin/getUsersCSV`, "Users.csv")
      // this.successToast(response.message);

    // })
  }

  onChange(e, id) {
    const data =
    {
      "id": id
    }
    this.SrvAuth.disabledUsers(data).subscribe((res: any) => {
      this.getAllUsers('', '')
    })
  }

  openSmallModal(smallModalContent,id) {
    alert(id)
    this.userid = id
    this.modalService.open(smallModalContent, { size: 'sm' });
this.viewdata(this.userid)
  }

  viewdata(id)
  {
    const data=
    {
      "id":id
    }
    this.SrvAuth.viewUserdata(data).subscribe((res:any)=>
    {
      console.log(res);
    })
  }

  openEditModal(EditModalContent, id) {
    this.userid = id;
    this.Edituserdata = this.userdata.filter(ele => ele._id == id)
    this.Editprofile.patchValue(this.Edituserdata[0])
    console.log(this.Edituserdata);
    this.modalService.open(EditModalContent, { size: 'lg' });

  }

  SubmitUser() {
    // dd/yyyy/mm
   var x = moment(this.Editprofile.controls['dob'].value).format('MM/DD/YYYY');
   this.Editprofile.controls['dob'].setValue(x)
   this.Editprofile.controls['id'].setValue(this.userid)
   console.log(this.Editprofile)
   this.SrvAuth.submitEdituser(this.Editprofile.value).subscribe((res: any) => {
    this.getAllUsers('', '')
    })
  }

  deleteUser(id) {
    const data =
    {
      "id": id
    }
    this.SrvAuth.deleteUser(data).subscribe((res: any) => {
      if (res.success) {
        //this.toaster.success("Deleted successfully!")
        this.getAllUsers('', '')
      }

    })
  }

  close(EditModalContent)
  {
    //this.modalService.close(EditModalContent);
  }

  alphabate(event)
  {
   // alert(event.keyCode)
    const charCode = (event.which) ? event.which : event.keyCode;
    if ((charCode > 65 && charCode < 90) || (charCode > 97 && charCode < 122)) {
      return true;
    }
    return false;
  }
}
