import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  name:any;
  password:any;
  constructor(private Authsrv:ServiceService) { }

  ngOnInit() {
  }

  login()
  {
    const body ={
      "email":this.name,
      "password":this.password
    }
    this.Authsrv.Login(body).subscribe((res:any)=>
    {
     if(res.success)
     {
       sessionStorage.setItem('Islogin','true')
     }
     else
     {
      sessionStorage.setItem('Islogin','false')
     }
    })
  }
}
