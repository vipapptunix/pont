import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/service.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  forgotemail:any;
  constructor(private SrvAuth:ServiceService) { }

  ngOnInit() {
  }

  forgotSubmit()
  {
    const data=
    {
      "email":this.forgotemail
    }
    this.SrvAuth.forgotEmail(data).subscribe((res:any)=>
    {

    })
  }  
}
